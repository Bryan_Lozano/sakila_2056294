<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.css" integrity="sha512-mG7Xo6XLlQ13JGPQLgLxI7bz8QlErrsE9rYQDRgF+6AlQHm9Tn5bh/vaIKxBmM9mULPC6yizAhEmKyGgNHCIvg==" crossorigin="anonymous" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sakila - Lista de Categorias</title>
</head>
<body>
    <h1>Lista de categorias</h1>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>
                    Nombre de categoria 
                </th>
                <th>
                    Actualizar
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($categorias as $c)
                <tr>
                    <th>
                        {{ $c->name }}
                    </th>
                    <td>
                        <a href="{{url('categorias/edit/'.$c->category_id )}}">
                            Actualizar
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{$categorias->links()}}
</body>
</html>