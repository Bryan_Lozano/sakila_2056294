<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Categoria;
use Illuminate\Support\Facades\Validator;

class CategoriaController extends Controller
{
    //Accion: Metoddo del controlador
    //Nombre puede ser cualquiera 
    //Recomendado el nombre en minuscula
    public function index(){

        //Prueba xd 
        //echo "Lista de Categoria";

        //Seleccionar las categorias existentes
        //"all" es que traiga todos los datos
        //"paginate" es que los trae de poco en poco, en este caso (5) 
        $categorias = Categoria::paginate(5);
        
        //Recorrer cada categoria
        /**foreach($categorias as $c){
           
            echo "<pre>";
            var_dump($c->name);
            echo "</pre><hr />";

        }
        */
        
        //Enviar la colexion de categorias a una vista y las vamos a mostrar alli 
        return view("categorias.index")->with("categorias" , $categorias);
    
    } 

    //Mostrar el dormulario de crar categpria 
    public function create(){
        
        //echo "Formulario de Categoria";
        return view("categorias.new");
        
    }

    //Llegar los datos desde el formulario
    //Guardar la categoria en BD
    public function store(Request $r){    

        //$_POST= Es un arreglo de php, que almacena la informacion que proceda de formularios 
        var_dump($_POST);

        //Validacion
        //1. Establecer las reglas de validacion para cada campo
        $reglas = [
            "categoria" => ["required" , "alpha"]
        ];

        $mensajes = [
            "required" => "Campo OBLIGATORIO",
            "alpha" => "Solo letras"
        ];
        //2.Crear el objeto Validador
        $validador = Validator::make($r->all() , $reglas , $mensajes );

        //3. Validar: Metodo fails
        //Retorna true(V) si la validacion falla 
        //Retorna falso en caso de que los datos sean correctos

        if($validador->fails()){
            //Codigo para cuando falla  
            return redirect("categorias/create")->withErrors($validador);         
        }else{
            //Codigo cuando la validacione s correcta
        }

        //Crear una nueva categoria
        $categoria = new Categoria();

        //Asignamos el nombre a la categoria 
        //trae datos desde el campo del formulario "categoria"
        $categoria->name = $r->input("categoria");

        //Guardar la nueva categoria
        $categoria->save();
        
        //Letrero de exito
        echo "Categoria guardada";

        //Redireccion con datos de sesion:
        return redirect('categorias/create')->with("mensaje" , "Categoria Guardada");

    }

    public function edit($category_id){
        //Seleccionar la categoria a editar
        $categoria = Categoria::find($category_id);

        //mostrar la vista de actualizar categoria 
        //llevando dentro la categoria
        return view("categorias.edit")->with("categoria", $categoria);
    }

    public function update($category_id){
        //Seleccionar la categoria a editar
        $categoria = Categoria::find($category_id);
        //Editar sus atributos
        $categoria->name = $_POST["categoria"];
        //guardar cambios
        $categoria->save();
        //Retornar formulario de editar
        return redirect("categorias/edit/$category_id")->with("mensaje" , "Categoria editada");
    } 
}
